"""
Serve webcam images from a Redis store using Tornado.
Usage:
   python server.py
"""

import base64
import os

import cv2
import socketio
from tornado import websocket, web, ioloop

from configuration import TORANDO_PORT
from face_recognition_handler import FaceRecognition
from ir_handler import LeptonCamera
from loggers import logger
from rgb_handler import VideoCamera

# 12/19測試 socketio 包裝成的 server bj6kc6g3m/4
# https://github.com/miguelgrinberg/python-socketio/issues/201
sio = socketio.AsyncServer(async_mode="tornado")
_Handler = socketio.get_tornado_handler(sio)


class IndexHandler(web.RequestHandler):
    """ Handler for the root static page. """

    def get(self):
        self.render('index.html')


class TestViewHandler(web.RequestHandler):
    """ Handler for the root static page. """

    def get(self):
        self.render('testView.html')


class MergeSocketHandler(websocket.WebSocketHandler):
    global rgb_handler, ir_handler, face_recognition_handler

    def cv2_base64(self, image):
        base64_str = cv2.imencode('.jpg', image)[1].tostring()
        base64_str = base64.b64encode(base64_str)
        return base64_str

    def __init__(self, *args, **kwargs):
        """ Initialize the Redis store and framerate monitor. """
        super(MergeSocketHandler, self).__init__(*args, **kwargs)
        self.rgb_handler = rgb_handler
        self.ir_handler = ir_handler
        self.face_recognition_handler = face_recognition_handler


    def on_message(self, message):
        image = self.rgb_handler.get_current_frame()
        #image = self.ir_handler.get_current_frame()
        # print(type(image))
        id_list, location_list, temp_list, temp_location_list = self.face_recognition_handler.get_label_data()
        try:
            for index, id in enumerate(id_list):
                location = location_list[index]
                x1 = location[3]
                y1 = location[0]
                x2 = location[1]
                y2 = location[2]
                red_color = (0, 0, 255)  # BGR
                cv2.rectangle(image, (x1, y1), (x2, y2), red_color, 1, 1)
#                location = temp_location_list[index]
#                x1 = location[3]
#                y1 = location[0]
#                x2 = location[1]
#                y2 = location[2]
#                red_color = (255, 0, 255)  # BGR
#                cv2.rectangle(image, (x1, y1), (x2, y2), red_color, 1, 1)
                temp = temp_list[index]
                cv2.putText(image, "Tem:" + str(temp), (x2+5, y2), cv2.FONT_HERSHEY_SIMPLEX,
                            0.5, (0, 0, 255), 1, 1)
                cv2.putText(image, "ID:" + str(id), (x2+5, y2-15), cv2.FONT_HERSHEY_SIMPLEX,
                            0.5, (0, 0, 255), 1, 1)
        except Exception as e:
            pass

        base64_image = self.cv2_base64(image=image)
        #base64_image = self.cv2_base64(image=self.ir_handler.get_current_frame())

        # print(image)
        self.write_message(base64_image)

def main():
    global rgb_handler, ir_handler, face_recognition_handler
    rgb_handler = VideoCamera()
    rgb_handler.start_handler()
    ir_handler = LeptonCamera()
    ir_handler.start_handler()
    face_recognition_handler = FaceRecognition(rgb_handler=rgb_handler, ir_handler=ir_handler)
    face_recognition_handler.start_handler()


    app = web.Application(
        [
            (r'/', IndexHandler),
            (r'/testView', TestViewHandler),
            (r'/mergeSocket', MergeSocketHandler),
        ],
        # cookie_secret="__TODO:_GENERATE_YOUR_OWN_RANDOM_VALUE_HERE__",
        template_path=os.path.join(os.path.dirname(__file__), "templates"),
        static_path=os.path.join(os.path.dirname(__file__), "static"),
        # xsrf_cookies=True,
        # debug=options.debug,
        debug=True,
        gzip=True,
    )
    app.listen(TORANDO_PORT)
    logger.info("Tornado server listen on {}".format(TORANDO_PORT))

    ioloop.IOLoop.current().start()
    logger.info("Tornado server start")


rgb_handler = None
ir_handler = None
face_recognition_handler = None
if __name__ == "__main__":
    main()
