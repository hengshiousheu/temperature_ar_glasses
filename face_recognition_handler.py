import math
import threading
import time

import face_recognition
import numpy as np

from configuration import PI_FRAME_SIZE_WEIGHT, PI_FRAME_SIZE_HEIGHT
from ir_handler import LeptonCamera
from rgb_handler import VideoCamera


class FaceRecognition(object):
    rgb_handler = None
    ir_handler = None
    runner = None
    run_flag = False

    temp_list = []
    location_list = []
    temp_location_list = []
    id_list = []
    update_time = 0
    detect_time_out = 0.5
    def __init__(self, rgb_handler: VideoCamera, ir_handler: LeptonCamera, detect_time_out=0.5):
        self.rgb_handler = rgb_handler
        self.ir_handler = ir_handler
        self.runner = threading.Thread(target=self.thread_process, daemon=True)
        self.update_time = time.time()
        self.detect_time_out = detect_time_out

    def __del__(self):
        self.run_flag = False

    def start_handler(self):
        self.run_flag = True
        self.runner.start()

    def compute_temp_from_frame(self, lepton_frame_process, face_locations):
        temp = []
        temp_location_list = []
        for face_location_tuple in face_locations:
            for (top, right, bottom, left) in face_location_tuple:
                #right, top = self.camera_distortion(x=right, y=top)
                #left, bottom = self.camera_distortion(x=left, y=bottom)
                x_c =  (right + left)/2
                x_d = (right - left)/3
                y_c =  (bottom + top)/2
                y_d = (bottom - top)/3
                 
                top = y_c - y_d*1.5
                bottom = y_c - y_d*0.5
                left = x_c - x_d
                right = x_c + x_d
                
                box = np.array([top, right, bottom, left])
                (top, right, bottom, left) = box.astype("int")
                roi = lepton_frame_process[top:bottom, left:right]
                flatten_roi = roi.flatten()
                if not flatten_roi.size:
                    continue
                flatten_roi = [ i for i in flatten_roi if type(i).__name__ == "float64" ]
                flatten_roi = [ i for i in flatten_roi if i > 34 ]
               
                sorted_flatten_roi = np.sort(flatten_roi)
                _temp = (sorted_flatten_roi[0] + sorted_flatten_roi[-1])/2
                #_temp = np.median(np.sort(flatten_roi)[:-5])
                if _temp < 33 or math.isnan(_temp):
                    _temp = -1
                _temp = round(_temp, 2)
                temp.append(_temp)
                temp_location_list.append([top, right, bottom, left])
        return temp, temp_location_list

    def get_label_data(self):
        return self.id_list, self.location_list, self.temp_list, self.temp_location_list

    def camera_distortion(self, x, y, thermal_shape=(320, 240), h=0.9, v=1.012, im_x=PI_FRAME_SIZE_WEIGHT,
                          im_y=PI_FRAME_SIZE_HEIGHT):
        x_ratio = 1.8
        y_ratio = 2.024
        x_bias = -16
        y_bias = 1.44
        x = (x + x_bias) / x_ratio
        y = (y + y_bias) / y_ratio
        x = max(x, 0)
        y = max(y, 0)
        return int(x), int(y)

    def thread_process(self):
        while self.rgb_handler.get_current_frame() is None or self.ir_handler.get_current_frame() is None:
            continue

        while self.run_flag:
            _temp_list = []
            _location_list = []
            _id_list = []
            _temp_location_list = []

            rgb_frame = self.rgb_handler.get_current_frame()
            ir_frame = self.ir_handler.get_current_frame()

            # small_rgb_frame = cv2.resize(rgb_frame, (0, 0), fx=0.25, fy=0.25)
            small_rgb_frame = rgb_frame

            # Detect if people or not
            X_face_locations = face_recognition.face_locations(small_rgb_frame)
            if len(X_face_locations) is 0:
                # if int(str(time.time()).split(".", )[0]) - self.update_time > 1:
                if time.time() - self.update_time > self.detect_time_out:
                    self.temp_list = []
                    self.location_list = []
                    self.temp_location_list = []
                    self.id_list = []
                # print("Has no face!")
                continue

            for index, location in enumerate(X_face_locations):
                (a, b, c, d) = location
                _location_list.append([a, b, c, d])
                _id_list.append(index + 1)

            _temp_list, _temp_location_list = self.compute_temp_from_frame(ir_frame, [X_face_locations])
            
            self.temp_location_list = _temp_location_list
            self.temp_list = _temp_list
            self.location_list = _location_list
            self.id_list = _id_list
            # self.update_time = int(str(time.time()).split(".", )[0])
            self.update_time = time.time()

# logger.info({
#     'id_list': str(self.id_list),
#     'location_list': str(self.location_list),
#     'temp_list': str(self.temp_list),
#     'unixtime': self.update_time
# })
